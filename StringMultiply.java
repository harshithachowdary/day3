class Solution {
    public String multiply(String num1, String num2) {
        if(num1.equals("0") || num2.equals("0")) return "0";
        
        int  len1 = num1.length();
        int  len2 = num2.length();
        int  len = len1+len2-1;
        int [] lc = new int[len];
        int carry=0;
		StringBuilder result = new StringBuilder(len+1); 
        
        for(int i=len1-1; i>=0; i--)
        {
            for(int j=len2-1; j>=0; j--)
            {
                int x = num1.charAt(i) - '0';
                int y = num2.charAt(j) - '0';
                lc[i+j] += x * y;
            }
         }
        
        for(int i=(len-1); i>=0; i--)
        {
            lc[i] += carry;
            result.insert(0,lc[i]%10);
            carry = lc[i]/10;
        }
          
        if(carry>0) result.insert(0,carry);
        
        return result.toString(); 
    }
}
        
    
