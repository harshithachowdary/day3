class Solution {
    public void nextPermutation(int[] nums) {
          int len = nums.length;
        
            if(len<=1)
                return;
        
            if(len==2){
                int temp = nums[0];
                nums[0] = nums[1];
                nums[1] = temp;
                return;
            }
        
        int index1 = len-2;//first index to be swapped
        boolean isPermutationHighest = true;
        
        	//check if permutation is already highest or not
	        for(; index1>=0; index1--) {
	        	if(nums[index1]<nums[index1 + 1]) {
	        		isPermutationHighest = false;
	        		break;
	        	}
	        }
	        
	       if(isPermutationHighest) {//means permutation is already highest
	    	   Arrays.sort(nums);
	    	   return;
	       }
	       
	       int index2 = index1;//find immediate greater number's index from index1
	       boolean isFirstGreater = true;
	       
	       for(int traverse = index1; traverse<len; traverse++){
               //find the immediate greater number with its index
               if(nums[traverse]>nums[index1] && isFirstGreater==true){
                   isFirstGreater = false;
                   index2 = traverse;
               }
               else if(nums[traverse]>nums[index1] && nums[traverse]<nums[index2]){
                   index2 = traverse;
               }
           }
	       
	       //swap index1 with index2
	       int temp = nums[index1];
	       nums[index1] = nums[index2];
	       nums[index2] = temp;
	       
	       //sort the all elements after index1 in increasing order
	       Arrays.sort(nums,index1+1,len);

       
    }
}
      
