class Solution {
    public int ClimbStairs(int n) {
        if (n == 1) {
            return 1;
        }
        
        if (n == 2) {
            return 2;
        }
        
        int t = 1, r = 2;
        for (int i = 3; i <= n; i++) {
            int tmp = r;
            r = r + t;
            t = tmp;
        }
        
        return r;
    }
}
        
    