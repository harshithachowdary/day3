import java.util.*;  
class Solution {
    public int[][] merge(int[][] intervals) {
          Arrays.sort(intervals,(a,b)->a[0]-b[0]);
          Vector<int[]> res = new Vector<int[]>();
        
          int[] prev = intervals[0];
          int[] curr = new int[2];
          for(int i=1;i<intervals.length;i++)
        {
            curr = intervals[i];
            if(prev[1]>=curr[0]){
                prev[0]=Math.min(prev[0],curr[0]);
                prev[1]=Math.max(prev[1],curr[1]);
                
            }
            else{
                res.add(prev);
                prev=curr;               
            }
        }
        
        res.add(prev);
        
        return res.toArray(new int[res.size()][]);
    }
}
        
    
