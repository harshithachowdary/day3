class Solution {
    public int Reverse(int x) {
         int div = 10;
        int res = x % div;
        x /= div;      //do init
        while (x != 0) {
			//if overflow then return zero
            if ((x > 0 && (0x7fffffff - x % div) / div < res) ||
                (x < 0 && (0x80000000 - x % div) / div > res) ) return 0;
           
		    //do reverse
			res = res * div + x % div;
            x /= div;
        }
        return res;
    }
    }
