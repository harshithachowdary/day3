class Solution {
    public int arrayNesting(int[] nums) {
        int max = -1;
        Set<Integer> set = new HashSet<>();
        for (int num : nums){
            int len;
            for (len = 0; set.add(num = nums[num]); len++);
            max = java.lang.Math.max(max, len);
        }
        return max;
    }
}