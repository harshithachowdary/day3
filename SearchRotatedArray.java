class Solution {
    public int searchRotatedArray(int[] nums, int target) {
          int forward = 0, backword = (nums.length-1);
        if(backword < 0) return -1;
        
        while(forward < backword && (forward+1) != backword ){
            int pivort = forward + (int) ((backword-forward)*.5);
            if(nums[forward] <= nums[pivort]){
                if(target >= nums[forward] && target <= nums[pivort]) backword = pivort;
                else forward = pivort;
            }else if(nums[pivort] <= nums[backword] ){
                 if(target <= nums[backword] && target >= nums[pivort]) forward = pivort;
                else backword = pivort;
            }
        }
        
        if(nums[forward] == target) return forward;
        if(nums[backword] == target) return backword;
        
     return -1;
    }
}
    
